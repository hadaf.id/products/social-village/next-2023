import { PrismaClient } from "@prisma/client";
import dayjs from "dayjs";
import { NextResponse } from "next/server";
import { v4 } from "uuid"

const textToSlug = (text: string) => text.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '')

export async function GET(
  request: Request,
  { params }: { params: { villageSlug: string } }
) {

  const prisma = new PrismaClient()

  const news = await prisma.konten.findMany({
    select: {
      url: true,
      judul: true,
      foto: true,
      kode_desa: true,
      waktu_publikasi: true,
    },
    where: {
      desa: {
        url: params.villageSlug
      }
    }
  })

  return NextResponse.json(news)
}

export async function POST(
  request: Request,
  { params }: { params: { villageSlug: string } }
) {

  const formData = await request.formData()
  const title = formData.get('title')?.toString()
  const content = formData.get('content')?.toString()
  const content_type_id = Number(formData.get('content_type_id')) 

  const prisma = new PrismaClient()

  const village = await prisma.desa.findUnique({
    where: {
      url: params.villageSlug,
    }
  })

  console.log("village", village?.kode)

  const news = await prisma.konten.create({
    data: {
      uuid: v4(),
      judul: title,
      isi: content,
      url: textToSlug(title ? title : ''),
      waktu_publikasi: dayjs().format(),
      kode_desa: village?.kode,
      id_tipe_konten: content_type_id, 
    }
  })

  return NextResponse.json({
    "thank_you": "much appreciated",
  })
}
