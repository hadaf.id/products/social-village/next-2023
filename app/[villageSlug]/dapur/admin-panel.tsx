'use client'

import Image from "next/image"
import Link from "next/link"
import { ReactNode } from "react"
import { FaChartPie, FaNewspaper, FaStore, FaMapMarked, FaUserFriends, FaMailBulk, FaChair, FaCampground, FaShoppingBasket, FaExclamation } from "react-icons/fa"

interface Props {
  params: { villageSlug: string }
  children: ReactNode
  title?: ReactNode 
}

const AdminPanel = ({ params, children, title }: Props) => {
 return (
  <div className="w-full">
    <div className="z-20 fixed flex w-full justify-start py-1 px-2 bg-teal-900">
      <div className="flex flex-row items-center gap-2">
        <div className="flex">
          <Link href={`/${params.villageSlug}`}>
            <Image
              className="transition duration-300 scale-75 group-hover:scale-100"
              src={`/regions/provinces/32/3205/logo.png`}
              alt={`Logo Desa Karyamukti`}
              width={24}
              height={0}
            />
          </Link>
        </div>
        <div className="flex flex-col">
          <h1 className="text-sm font-heading font-bold text-white">
            Admin Portal Desa Karyamukti
          </h1>
        </div>
      </div>
    </div>
    <div className="z-10 fixed flex bg-teal-800 w-60 pt-12 h-screen">
      <div className="flex flex-col w-full">
        {[
          {
            title: [<FaChartPie />, <span>Dashboard</span>],
            url: `/${params.villageSlug}/dapur/dashboard`,
          },
          {
            title: [<FaStore />, <span>Profil Desa</span>],
            url: `/${params.villageSlug}/dapur/profil`,
          },
          {
            title: [<FaNewspaper />, <span>Berita Kegiatan</span>],
            url: `/${params.villageSlug}/dapur/berita`,
          },
          {
            title: [<FaUserFriends />, <span>Kependudukan</span>],
            url: `/${params.villageSlug}/dapur/kependudukan`,
          },
          {
            title: [<FaMailBulk />, <span>Persuratan</span>],
            url: `/${params.villageSlug}/dapur/persuratan`,
          },
          {
            title: [<FaCampground />, <span>Pariwisata Unggulan</span>],
            url: `/${params.villageSlug}/dapur/pariwisata`,
          },
          {
            title: [<FaShoppingBasket />, <span>Produk Unggulan</span>],
            url: `/${params.villageSlug}/dapur/produk`,
          },
          {
            title: [<FaMapMarked />, <span>Peta Fasum / Fasos</span>],
            url: `/${params.villageSlug}/dapur/fasumfasos`,
          },
          {
            title: [<FaExclamation />, <span>Keluhan & Usulan Warga</span>],
            url: `/${params.villageSlug}/dapur/lapor`,
          },
        ].map((item, key) => {
          return (
            <Link
              key={`sidebar-menu-item-${key}`}
              className={`flex flex-row items-center gap-2 px-2 py-1 hover:bg-teal-900 w-full
                transition duration-300 font-heading text-teal-200 hover:text-amber-200
                hover:border-solid border-l-4 hover:border-amber-200 border-teal-800`}
                href={item.url}
            >
              {item.title}
            </Link>
          )
        })}
      </div>
    </div>
    <div className={`flex flex-col w-full pl-60 pt-16 items-center`}>
      <div className="flex flex-col w-10/12 xl:w-11/12">
        <h1 className="font-heading text-3xl font-bold text-teal-900 mb-8">{title}</h1>
        {children}
      </div>
    </div>
  </div>
 )
}

export default AdminPanel
