import AdminPanel from "../admin-panel"

type Props = {
  params: { villageSlug: string }
}

export default ({ params }: Props) => {
  return (
    <main className="flex flex-col items-start bg-gray-100 min-h-screen">
      <AdminPanel params={params} title="Pariwisata Unggulan">      
      </AdminPanel>
    </main>
  )
}
