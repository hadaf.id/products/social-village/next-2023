import AdminPanel from "../admin-panel"
import ProfileClient from "./client"

type Props = {
  params: { villageSlug: string }
}

export default ({ params }: Props) => {
  return (
    <main className="flex flex-col items-start bg-gray-100 min-h-screen">
      <AdminPanel params={params} title="Profil Desa">      
        <ProfileClient /> 
      </AdminPanel>
    </main>
  )
}
