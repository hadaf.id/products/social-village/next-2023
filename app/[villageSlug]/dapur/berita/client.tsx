'use client'

import { Editor } from "@tinymce/tinymce-react"
import Link from "next/link"
import { useEffect, useRef, useState } from "react"
import { FaCamera, FaPlay, FaPlus } from "react-icons/fa"
import { Editor as TinyMCEEditor } from "tinymce"
import Image from "next/image"


interface Props {
  villageSlug: string
}

interface CreateNews {
  open: boolean
  title: string
  content: string
  photo?: File 
  photoPreviewSrc?: string | ArrayBuffer | null
  content_type_id: number 
  village_code: string
}

const NewsClient = ({ villageSlug }: Props) => {

  const [createNews, setCreateNews] = useState<CreateNews>({
    open: false,
    title: '',
    content: '',
    content_type_id: 2,
    village_code: '',
  })

  const [news, setNews] = useState<{
    url: string,
    judul: string,
    foto?: string,
    kode_desa: string,
    waktu_publikasi: string,
  }[]>([])

  const getNews = () => {
    fetch(`/${villageSlug}/api/news`)
      .then(res => res.json())
      .then(res => {
        setNews(res)
      })
  }

  useEffect(() => {
    getNews()
  }, [])

  const editorRef = useRef<TinyMCEEditor | null>(null)

  const selectPhotoHandler = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const filesFromInput = e.target.files
    if(filesFromInput !== null && filesFromInput.length === 1) {
      const photo = Array.from(filesFromInput)[0]
      setCreateNews(v => ({...v, photo: photo}))

      const fotoReader = new FileReader()

      fotoReader.onload = e => {
        const photoPreviewSrc = e.target
        if(photoPreviewSrc !== null) {
          setCreateNews(v => ({...v, photoPreviewSrc: photoPreviewSrc.result}))
        }
      }

      fotoReader.readAsDataURL(photo)
    }
  }

  const onSubmit = () => {
    const formData = new FormData()

    formData.append('content_type_id', createNews.content_type_id.toString())
    formData.append('title', createNews.title)
    formData.append('content', createNews.content)

    if(createNews.photo) {
      formData.append('photo', createNews.photo)
    }

    fetch(
      `/${villageSlug}/api/news`,
      {
        method: 'POST',
        body: formData,
      }
    )
      .then(res => res.json())
      .then(res => {
        console.log("berhasil posting")
        getNews()
      })

  }


  return (
    <div className="w-full flex flex-col gap-8">
      <div className="gap-4">

        {false &&
        <button
          className="bg-teal-700 hover:bg-teal-800 text-white font-bold py-2 px-4 rounded"
          onClick={() => {
            setCreateNews(v => ({...v, open: true}))
          }}
        >
          <div className="flex flex-row items-center gap-2">
            <FaPlus /> Buat Berita Baru
          </div>
        </button>
        }
          <div
            className={`gap-4 flex flex-col w-full transition duration-300`}
          >
            <h2 className="font-heading text-xl font-bold text-teal-900">Berita Baru</h2>
            <div className="w-full flex flex-col shadow-lg rounded p-4 gap-4">
              <div className="w-full flex justify-end border-b pb-4">
                <button
                  className="bg-teal-700 hover:bg-teal-800 text-white font-bold py-2 px-4 rounded"
                  onClick={onSubmit}
                >
                  <div className="flex flex-row items-center gap-2">
                    <FaPlay /> Publikasikan 
                  </div>
                </button>
              </div>
              <div className="w-full flex flex-row gap-4">
                <div className="grow flex flex-col gap-4">
                  <div className="w-full flex flex-col">
                    <label className="block text-gray-700 text-sm font-bold mb-2">
                      Judul Berita
                    </label>
                    <input
                      className="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      value={createNews.title}
                      onChange={e => {
                        setCreateNews(v => ({...v, title: e.target.value}))
                      }}
                    />
                  </div>
                  <Editor
                    onInit={(evt, editor) => {
                      editorRef.current = editor
                    }}
                    apiKey={`lo2hlnldeodnoxcdx66l5s7dw1xcax64kolvz6zd1gvgkvgk`}
                    initialValue="<p>Isi berita ...</p>" 
                    init={{
                      menubar: false,
                      width: "100%",
                      plugins: ['autoresize'],
                    }}
                    onEditorChange={content => {
                      setCreateNews(v => ({...v, content: content}))
                    }}
                  />
                  <div className="w-full flex flex-col">
                    <label className="block text-gray-700 text-sm font-bold mb-2">
                      Kategori
                    </label>
                    <div className="w-full flex">
                      {[
                        {
                          label: 'Kegiatan',
                        },
                        {
                          label: 'PKK',
                        },
                        {
                          label: 'Karang Taruna',
                        },
                      ].map((item, key) => {
                        return (
                          <div className="mb-[0.125rem] mr-4 inline-block min-h-[1.5rem]">
                            <input
                              type="checkbox"
                            />
                            <label
                              className="inline-block pl-[0.15rem] hover:cursor-pointer"
                            >
                              {item.label}
                            </label>
                          </div>
                        )
                      })}
                    </div>
                  </div>
                </div>
                <div className="w-64 flex flex-col gap-2">
                  <label className="block text-gray-700 text-sm font-bold">
                    Upload Foto
                  </label>
                  <label className="bg-gray-500 hover:bg-gray-600 cursor-pointer text-sm text-white font-bold py-2 px-4 rounded" htmlFor="select-file-button">
                    <div className="flex flex-row items-center gap-2">
                      <FaCamera /> Pilih file 
                    </div>
                    <input accept="image/*" id="select-file-button" className="hidden" type="file" onChange={selectPhotoHandler} />
                  </label>
                  {createNews.photoPreviewSrc !== undefined && typeof createNews.photoPreviewSrc === 'string' &&
                  <img
                    alt="Uploaded photo"
                    src={createNews.photoPreviewSrc}     
                  />
                  }
                </div>
              </div>
            </div>
          </div>
      </div>
      <div className="w-full flex flex-col gap-4">
        <h2 className="font-heading text-xl font-bold text-teal-900">Arsip Berita</h2>
        <div className="w-full flex shadow-lg rounded">
          <table className="table-auto w-full text-left text-sm font-light">
            <thead className="border-b font-medium dark:border-teal-800">
              <tr>
                <th className="px-6 py-4">
                  Waktu Publikasi
                </th>
                <th className="px-6 py-4">
                  Judul
                </th>
              </tr>
            </thead>
            <tbody>
              {news.map((item, key) => {
                return (
                  <tr className={`border-b transition duration-300 ease-in-out hover:bg-amber-50 dark:border-gray-300 dark:hover:bg-amber-50`}>
                    <td className="whitespace-nowrap px-6">{item.waktu_publikasi}</td>
                    <td className="whitespace-nowrap">
                      <Link
                        className="flex py-4"
                        href={`/${villageSlug}/dapur/berita/${item.url}`}
                      >
                        {item.judul}
                      </Link>
                    </td>
                  </tr>
                )
              })} 
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default NewsClient
