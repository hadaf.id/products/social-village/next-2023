import { FaChartPie } from "react-icons/fa"
import AdminPanel from "../admin-panel"
import DashboardClient from "./client"

type Props = {
  params: { villageSlug: string }
}

export default ({ params }: Props) => {
  return (
    <main className="flex flex-col items-start bg-gray-100 min-h-screen">
      <AdminPanel params={params} title={<div className="flex flex-row items-center gap-2"><FaChartPie /> Dashboard</div>}>      
        <DashboardClient />
      </AdminPanel>
    </main>
  )
}
