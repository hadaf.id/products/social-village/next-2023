import AdminPanel from "./admin-panel"
import Link from "next/link"

type Props = {
  params: { villageSlug: string }
}

export default ({ params }: Props) => {
  return (
    <main className="flex flex-row items-start bg-gray-100 min-h-screen">
      <div className="flex flex-col h-screen w-80 justify-center items-center bg-gray-200">
        <div className="flex flex-col gap-4">
          <h1>Login Portal Desa {params.villageSlug}</h1>
          <input className="p-2" placeholder="username" />
          <input type="password" className="p-2" placeholder="password" />
          <Link href={`/${params.villageSlug}/dapur/dashboard`}>
            <button className="bg-teal-700 hover:bg-teal-800 text-white font-bold py-2 px-4 rounded" >
              Login
            </button>
          </Link>
        </div>

      </div>
      <div className="flex flex-row h-full">
        {/* <h1>Login Portal Desa {params.villageSlug}</h1> */}

      </div>
    </main>
  )
}
