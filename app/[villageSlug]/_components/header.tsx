import Image from "next/image"
import Link from "next/link"
import { FaHome, FaKey } from "react-icons/fa"

type Props = {
  villageSlug: string
}

const Header = ({ villageSlug }: Props) => {
  return (
    <div className="flex flex-col w-full">
      <div className="flex w-full justify-start justify-center">
        <div className="flex h-16 w-full xl:w-10/12 2xl:w-10/12 justify-start py-12">
          <div className="flex flex-row items-center">
            <div className="flex">
              <Image
                className="transition duration-300 scale-75 group-hover:scale-100"
                src={`/regions/provinces/32/3205/logo.png`}
                alt={`Logo Desa Karyamukti`}
                width={64}
                height={0}
              />
            </div>
            <div className="flex flex-col">
              <h1 className="text-xl uppercase font-bold font-display">
                Portal Desa Karyamukti
              </h1>
              <h2 className="text-m uppercase font-bold font-display">
                Kec. Cibatu Kab. Garut Prov. Jawa Barat
              </h2>
            </div>
          </div>
        </div>
      </div>
      <div className="flex w-full justify-start bg-teal-700 justify-center">
        <div className="flex flex-row w-full xl:w-10/12 2xl:w-10/12 justify-start py-4 text-teal-200 font-display uppercase">
            <div className="grow flex flex-row gap-8">
              {[
                {
                  title: <div className="flex flex-row items-center gap-2"><FaHome /> Beranda</div>,
                  url: `/${villageSlug}/`,
                },
                {
                  title: <div className="flex flex-row items-center gap-2">Profil</div>,
                  url: `/${villageSlug}/halaman/profil`,
                },
                {
                  title: <div>Data</div>,
                  url: `/${villageSlug}/halaman/data`,
                },
                {
                  title: <div className="flex flex-row items-center gap-2">Berita</div>,
                  url: `/${villageSlug}/berita`,
                },
              ].map((item, key) => {
                return (
                  <Link href={item.url} className="flex hover:text-white">
                    <div className="flex flex-row items-center gap-2">
                      {item.title} 
                    </div>
                  </Link>
                )
              })}
            </div>
            <Link href={`/${villageSlug}/dapur`} className="flex hover:text-white">
              <div className="flex flex-row items-center gap-2">
                <FaKey /> Login
              </div>
            </Link>
        </div>
      </div>
    </div>
  )
}

export default Header
