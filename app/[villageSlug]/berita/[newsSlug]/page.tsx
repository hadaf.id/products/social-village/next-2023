import { Metadata } from "next"
import Header from "../../_components/header"

type Props = {
  params: {
    villageSlug: string,
    pageSlug: string,
  }
}

export const generateMetadata = async (
  props: Props
): Promise<Metadata> => {
  const { params } = props
  return {
    title: `${params.pageSlug} - Desa ${params.pageSlug}`,
  }
}

export default ({ params }: Props) => {
  return (
    <main className="flex flex-col items-center bg-gray-100 min-h-screen">
      <Header villageSlug={params.villageSlug} />      

      {/* Contents */}
      <div className="flex w-full justify-start justify-center">
        <div className="flex flex-col h-16 w-full xl:w-10/12 2xl:w-10/12 justify-start">
          <div className="flex justify-center py-12">
            <h1 className="font-bold text-5xl font-heading">Halaman</h1>
          </div>
          <div className="flex flex-col">
            Konten halaman
          </div>
        </div>
      </div>
    </main>
  )
}
