import { Metadata } from "next"
import Header from "./_components/header"
import { PrismaClient } from "@prisma/client"
import Link from "next/link"
import dayjs from "dayjs"
import "dayjs/locale/id"
import { FaBook, FaMailBulk } from "react-icons/fa"

dayjs.locale('id')

type Props = {
  params: { villageSlug: string }
}

export const generateMetadata = async (
  props: Props
): Promise<Metadata> => {
  const { params } = props
  return {
    title: `Desa ${params.villageSlug}`,
  }
}

export default async ({ params }: Props) => {

  const prisma = new PrismaClient()

  const news = await prisma.konten.findMany({
    select: {
      url: true,
      judul: true,
      foto: true,
      kode_desa: true,
      waktu_publikasi: true,
      isi: true,
    },
    where: {
      desa: {
        url: params.villageSlug
      }
    }
  })

  return (
    <main className="flex flex-col items-center bg-gray-100 min-h-screen">
      <Header villageSlug={params.villageSlug} />      

      <div className="w-full flex flex-col items-center py-8 bg-white">
        <div className="flex flex-col gap-8 items-center w-full xl:w-10/12 2xl:w-10/12 justify-start">
          <h2 className="text-4xl font-bold text-gray-700 font-heading">Layanan Publik</h2>
          <div className="w-full grid grid-cols-2 gap-4">
            <Link target="_blank" href={'https://pengabdi.net/karyamukti/'}>
              <div className="flex gap-4 flex-row rounded-md px-8 py-8 items-center justify-center bg-amber-200 hover:bg-amber-300 transition duration-300">
                <div className="text-2xl text-amber-900">
                  <FaMailBulk />
                </div>
                <h3 className="font-bold text-lg font-heading text-amber-900">Persuratan & Perizinan</h3> 
              </div>
            </Link>
            <Link target="_blank" passHref={true} href={'https://pengabdi.net/smartlibrary/'}>
              <div className="flex gap-4 flex-row rounded-md px-8 py-8 items-center justify-center bg-amber-200 hover:bg-amber-300 transition duration-300">
                <div className="text-2xl text-amber-900">
                  <FaBook />
                </div>
                <h3 className="font-bold text-lg font-heading text-amber-900">Perpustakaan Digital</h3> 
              </div>
            </Link>
          </div>
        </div>
      </div>

      <div className="w-full flex flex-col items-center py-8">
        <div className="flex flex-col gap-8 items-center w-full xl:w-10/12 2xl:w-10/12 justify-start">
          <h2 className="text-4xl font-bold text-gray-700 font-heading">Berita Terkini</h2>
          <div className="w-full grid grid-cols-2 gap-4">
            {news.map((item, key) => {

              const publishedTime = dayjs(item.waktu_publikasi)

              return (
                  <div
                    className="transition group duration-300 rounded p-4 gap-4 flex flex-row items-center overflow-hidden shadow bg-gray-50 hover:bg-white items-start"
                  >
                    <div className="flex w-full h-full flex-col text-lg gap-0 font-heading">
                      <Link href={item.url ? `/${params.villageSlug}/berita/${item.url}` : ''}>
                        <h3 className="font-bold text-md mb-2 hover:text-teal-800">{item.judul}</h3> 
                        <span>{publishedTime.format('DD MMMM YYYY')}</span>
                      </Link>
                      {/* {item.isi && */}
                      {/*   <div className="text-md mb-2" dangerouslySetInnerHTML={{__html: item.isi}}></div> */} 
                      {/* } */}
                    </div> 
                  </div>
              )
            })}
          </div>
        </div>
      </div>
    </main>
  )
}
