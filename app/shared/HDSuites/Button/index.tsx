import classNames from "classnames"
import { ReactNode } from "react"

interface Props {
  children: ReactNode
  bg?: string
  size?: "sm" | "md" | "lg"
}

export default ({children, bg, size}: Props) => {

  const _bg = bg ? bg : 'teal'

  return (
    <button className={classNames(`w-min transition duration-300 rounded text-white bg-${_bg}-700 hover:bg-${_bg}-800 py-2 px-4`, {
      "text-xs": size === 'sm',
      "text-md": size === 'sm' || size === undefined,
      "text-lg": size === 'lg',
    })}>
      {children}
    </button>
  )
}

// https://www.smashingmagazine.com/2020/05/reusable-react-components-tailwind/
