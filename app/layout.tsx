import './globals.css'
import type { Metadata } from 'next'
import { Oswald, Merriweather, PT_Sans } from 'next/font/google'

const ptsans = PT_Sans({
  weight: ['400', '700'],
  style: ['normal', 'italic'],
  subsets: ['latin'],
  display: 'swap',
  variable: '--font-ptsans',
})

const merriweather = Merriweather({
  weight: ['400', '700'],
  style: ['normal', 'italic'],
  subsets: ['latin'],
  display: 'swap',
  variable: '--font-merriweather',
})

const oswald = Oswald({
  weight: ['400', '700'],
  style: ['normal'],
  subsets: ['latin'],
  display: 'swap',
  variable: '--font-oswald',
})

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en" className='min-h-screen'>
      <body className={`font-heading ${oswald.variable} ${ptsans.variable} min-h-screen`}>
        {children}
      </body>
    </html>
  )
}
