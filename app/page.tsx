import { GetStaticProps, Metadata } from "next"
import SearchVillage from "./portal/search-village"
import Link from "next/link"
import Image from "next/image"

export const metadata: Metadata = {
  title: 'SGD Smart Village',
  description: 'Program Smart Village dari Teknik Informatika UIN Sunan Gunung Djati Bandung',
}

interface VillageLink {
  title: string
  description: string
  path: string
  icon: string
}

const getVillages = async (): Promise<VillageLink[]> => {

  const villages: VillageLink[] = [
    {
      title: 'Desa Karyamukti',
      description: 'Kec. Cibatu, Kab. Garut',
      path: 'karyamukti',
      icon: '/regions/provinces/32/3205/logo.png',
    },
    {
      title: 'Desa Sindangsuka',
      description: 'Kec. Cibatu, Kab. Garut',
      path: 'sindangsuka',
      icon: '/regions/provinces/32/3205/logo.png',
    },
    {
      title: 'Desa Wanaraja',
      description: 'Kec. Wanaraja, Kab. Garut',
      path: 'karyamukti',
      icon: '/regions/provinces/32/3205/logo.png',
    },
  ]

  return villages 
}

export default async () => {

  const villages = await getVillages()

  return (
    <main className="flex flex-col gap-8 items-center bg-gray-100 min-h-screen">
      <div className="flex pt-8 w-full xl:w-10/12 2xl:w-10/12 justify-center">
        <h1 className="text-5xl font-bold font-heading">
          SGD Smart Village
        </h1>
      </div>
      <div className="flex px-4 w-full xl:w-10/12 2xl:w-10/12 justify-center">
        <SearchVillage />
      </div>
      <div className="flex flex-col gap-4 px-4 w-full xl:w-10/12 2xl:w-10/12">
        <h2 className="text-xl font-bold text-gray-700 font-heading">Desa Populer</h2>
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
          {villages.map((item, key) => {
            return (
              <Link key={`popular-village-item-${key}`} href={item.path}>
                <div
                  className="transition group duration-300 rounded-xl p-4 gap-4 flex flex-row items-center overflow-hidden shadow bg-gray-50 hover:bg-white"
                >
                  <div className="flex">
                    <Image
                      className="transition duration-300 scale-75 group-hover:scale-100"
                      src={item.icon}
                      alt={`Logo ${item.title}`}
                      width={64}
                      height={0}
                    />
                  </div> 
                  <div className="flex flex-col gap-0 font-display">
                    <h3 className="font-bold text-md mb-2">{item.title}</h3> 
                    <p className="text-md mb-2">{item.description}</p> 
                  </div> 
                </div>
              </Link>
            )
          })}
        </div> 
      </div>
      <div className="flex px-4 w-full xl:w-10/12 2xl:w-10/12">
        <h2 className="text-xl font-bold text-gray-700 font-heading">Wisata Populer</h2>
      </div>
      <div className="flex px-4 w-full xl:w-10/12 2xl:w-10/12">
        <h2 className="text-xl font-bold text-gray-700 font-heading">Produk Unggulan</h2>
      </div>
      <div className="flex px-4 w-full xl:w-10/12 2xl:w-10/12">
        <h2 className="text-xl font-bold text-gray-700 font-heading">Berita Terkini</h2>
      </div>
    </main>
  )
}
