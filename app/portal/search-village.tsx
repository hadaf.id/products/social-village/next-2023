'use client'

import { useRouter } from "next/navigation"
import { useState } from "react"
import { FaSearch } from "react-icons/fa"

const SearchVillage = () => {
  const router = useRouter()
  const [keyword, setKeyword] = useState<string>('')

  return (
    <div className="relative">
      <input
        type="search"
        className="shadow rounded p-3 focus:ring-gray-200 focus:ring-2 focus:outline-none"
        placeholder="Cari desa..."
        value={keyword}
        onChange={e => {
          setKeyword(e.target.value)
        }}
      /> 
      {keyword.length > 2 &&
        <div
          onClick={() => {
            router.push(`/${keyword}`)
          }}
          className="absolute right-2 top-4 text-gray-600 hover:text-teal-600 hover:cursor-pointer"
        >
          <FaSearch />
        </div>
      }
    </div>
  )
}

export default SearchVillage
